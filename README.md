# packer builder vagrant virtualbox oracle linux 4 and 5

### Overview

This is a packer project that contains the scripts to build oracle linux 5.11 and 4.8 for vagrant for use with oracle 10.2 and oracle 9.2.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build any of these Vagrant boxes:

  - [Packer](http://www.packer.io/)
  - [Vagrant](http://vagrantup.com/)
  - [VirtualBox](https://www.virtualbox.org/)

## Building the Vagrant boxes with Packer

All variables in the json files.
To start the build, edit the 'b' file for the correct OS to build, and then execute:

$ ./b

The 'b' (build) file contains the build command for oracle linux 4 and 5.

## What does it do? (what does packer do)

Packer downloads the installation ISO,
sets up an http server and boots the installation ISO using virtualbox,
then runs kickstart to automatically install linux using a kickstart file from the http server,
then sets up the linux installation for use with vagrant (root password vagrant, vagrant user with password vagrant, and set the vagrant insecure key),
and the runs a couple of scripts to further configure the operating system for use with the oracle database.

## troubleshooting

logging
$ export PACKER_LOG=1
$ export PACKER_LOG_PATH="packerbuild.log"

ask
packer build -on-error=ask 

breakpoint; add to json file:
    {
      "type": "breakpoint"
    } ]
