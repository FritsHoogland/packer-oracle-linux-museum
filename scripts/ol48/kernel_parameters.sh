/bin/echo "add kernel parameters and activate them"
/bin/echo "kernel.shmmax = 2147483648
kernel.shmall = 2097152
kernel.shmmni = 100
kernel.sem = 100 256 100 100
kernel.hostname = $(/bin/hostname)
kernel.domainname = $(/bin/hostname -d)
fs.file-max = 327679" >> /etc/sysctl.conf
/sbin/sysctl -p
