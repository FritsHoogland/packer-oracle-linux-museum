echo "add kernel parameters and activate them"
echo "kernel.shmmni = 4096
      kernel.sem = 250 32000 100 128
      net.ipv4.ip_local_port_range = 1024 65000
      net.core.rmem_default = 262144
      net.core.rmem_max = 262144
      net.core.wmem_default = 262144
      net.core.wmem_max = 262144" >> /etc/sysctl.conf
/sbin/sysctl -p
