# below are general purpose OL versions
# packer fix: packer fix current.json > new.json
#
#packer build -on-error=ask centos_8_3_2011_x86_64.json
packer build -on-error=ask centos_8_3_2011_yb_rf1.json
#packer build -on-error=ask centos_8_3_2011_yb_rf3.json
#packer build -on-error=ask oracle_linux_8_3_x86_64.json
#packer build -on-error=ask oracle_linux_8_2_x86_64.json
#packer build -on-error=ask oracle_linux_8_1_x86_64.json
#packer build -on-error=ask oracle_linux_8_0_x86_64.json
#packer build -on-error=ask oracle_linux_7_9_x86_64.json
#packer build -on-error=ask oracle_linux_7_8_x86_64.json
#packer build -on-error=ask oracle_linux_7_7_x86_64.json
#packer build -on-error=ask oracle_linux_6_10_x86_64.json
# below are ancient OL versions target-built for running oracle
#packer build -on-error=ask oracle_linux_5_11_x86_64.json
#packer build -on-error=ask oracle_linux_4_8_x86.json
