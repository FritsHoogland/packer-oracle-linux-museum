#!/bin/sh -eux

# with OL8, the ol8_baseos_latest repo currently is the only repo that contains the oracle-database-preinstall-19c package.
# because this is aimed at installing the oracle database, that means we have to leave ol8_baseos_latest in.
# also, it seems ol8_appstream contains packages that are a dependency of the preinstall package. 
# the appstream is not version controlled per update/release, so ol8_appstream must be in too.
# both ol8_baseos_latest and ol8_appstream are enabled by default.
# for completeness sake I add ol8_u2_baseos_base, but this probably doesn't make sense because ol8_baseos_latest is enabled too.
#
#yum-config-manager --disable ol8_baseos_latest
#yum-config-manager --disable ol8_appstream
yum-config-manager --enable ol8_u3_baseos_base
