echo "set limits for nproc and nofile in limits.conf"
echo "* soft nproc 2047
* hard nproc 16384
* soft nofile 1024
* hard nofile 65536
* soft stack 10240
* hard stack 32768
* soft memlock 134217728
* hard memlock 134217728" >> /etc/security/limits.conf
