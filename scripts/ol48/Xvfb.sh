echo "Xvfb"
echo "#!/bin/bash
#
# chkconfig: 35 95 50
# description: Starts xvfb on display 99
#
if [ -z \"\$1\" ]; then
echo \"`basename $0` {start|stop}\"
    exit
fi

case \"\$1\" in
start)
    /usr/X11R6/bin/Xvfb :99 -screen 0 1280x1024x24 &
;;

stop)
    killall Xvfb
;;
esac" > /etc/init.d/Xvfb
chmod 755 /etc/init.d/Xvfb
/sbin/chkconfig --add Xvfb
